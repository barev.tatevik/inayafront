console.clear();
jQuery(document).ready(function ($) {
    //ALL Sliders
    try{
        var users_slider_container = new Swiper('.users_slider_container', {
            effect: 'coverflow',
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 'auto',
            coverflow: {
                rotate: 0,
                stretch: 0,
                depth: 0,
                modifier: 1,
                slideShadows : false
            },
            navigation: {
                nextEl: '.users_slider_next',
                prevEl: '.users_slider_prev',
            },
            loop: true,
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
        });


    }catch (e) {}

    try{
        var album_slider_container = new Swiper('.album_slider_container', {
            slidesPerView: 'auto',
            spaceBetween: 35,
            navigation: {
                nextEl: '.album_slider_next'
            },
        });
    }catch (e){}

    try{
        var post_type_container = new Swiper('.post_type_container', {
            slidesPerView: 'auto',
            spaceBetween: 35,
            navigation: {
                nextEl: '.post_type_next'
            },
        });
    }catch (e){}

    try{
        (function() {
            'use strict';
            const breakpoint = window.matchMedia( '(min-width:769px)' );
            let mySwiper;
            const breakpointChecker = function() {
                try{
                    if ( breakpoint.matches === true ) {
                        if ( mySwiper !== undefined ) mySwiper.destroy( true, true );
                        return;
                    } else if ( breakpoint.matches === false ) {
                        return enableSwiper();
                    }
                }catch(e){console.log(e)}
            };

            const enableSwiper = function() {
                try{
                    mySwiper = new Swiper ('.article_navbar_container', {
                        slidesPerView: 'auto',
                        spaceBetween: 15,
                        a11y: true,
                        keyboardControl: true,
                        navigation: {
                            nextEl: '.article_navbar_next',
                        }
                    });
                }catch(e){console.log(e)}
            };
            breakpoint.addListener(breakpointChecker);
            breakpointChecker();
        })();
    }catch (e){}

    try{
        (function() {
            'use strict';
            const breakpoint = window.matchMedia( '(min-width:769px)' );
            let mySwiper;
            const breakpointChecker = function() {
                try{
                    if ( breakpoint.matches === true ) {
                        if ( mySwiper !== undefined ) mySwiper.destroy( true, true );
                        return;
                    } else if ( breakpoint.matches === false ) {
                        return enableSwiper();
                    }
                }catch(e){console.log(e)}
            };

            const enableSwiper = function() {
                try{
                    mySwiper = new Swiper ('.interests_list_container', {
                        slidesPerView: 'auto',
                        spaceBetween: 15,
                        a11y: true,
                        keyboardControl: true,
                        navigation: {
                            nextEl: '.interests_list_next',
                        }
                    });
                }catch(e){console.log(e)}
            };
            breakpoint.addListener(breakpointChecker);
            breakpointChecker();
        })();
    }catch (e){}

    try{
        (function() {
            'use strict';
            const breakpoint = window.matchMedia( '(min-width:769px)' );
            let mySwiper;
            const breakpointChecker = function() {
                try{
                    if ( breakpoint.matches === true ) {
                        if ( mySwiper !== undefined ) mySwiper.destroy( true, true );
                        return;
                    } else if ( breakpoint.matches === false ) {
                        return enableSwiper();
                    }
                }catch(e){console.log(e)}
            };

            const enableSwiper = function() {
                try{
                    mySwiper = new Swiper ('.inspiration_list_container', {
                        slidesPerView: 'auto',
                        spaceBetween: 15,
                        a11y: true,
                        keyboardControl: true,
                        navigation: {
                            nextEl: '.inspiration_list_next',
                        }
                    });
                }catch(e){console.log(e)}
            };
            breakpoint.addListener(breakpointChecker);
            breakpointChecker();
        })();
    }catch (e){}

    try{
        (function() {
            'use strict';
            const breakpoint = window.matchMedia( '(min-width:769px)' );
            let mySwiper;
            const breakpointChecker = function() {
                try{
                    if ( breakpoint.matches === true ) {
                        if ( mySwiper !== undefined ) mySwiper.destroy( true, true );
                        return;
                    } else if ( breakpoint.matches === false ) {
                        return enableSwiper();
                    }
                }catch(e){console.log(e)}
            };

            const enableSwiper = function() {
                try{
                    mySwiper = new Swiper ('.post_rating_list_container', {
                        slidesPerView: 'auto',
                        spaceBetween: 15,
                        a11y: true,
                        keyboardControl: true,
                        navigation: {
                            nextEl: '.post_rating_list_next',
                        }
                    });
                }catch(e){console.log(e)}
            };
            breakpoint.addListener(breakpointChecker);
            breakpointChecker();
        })();
    }catch (e){}

    try{
        var friends_slider_container = new Swiper('.friends_slider_container', {
            slidesPerView: 'auto',
            spaceBetween: 35,
            navigation: {
                prevEl: '.friends_slider_prev',
                nextEl: '.friends_slider_next'
            },
        });
    }catch (e){}

    try{
        var stories_slider_container = new Swiper('.stories_slider_container', {
            pagination: {
                el: '.stories_pagination',
                clickable: true,
            },
            autoplay: {
                delay: 2500,
                disableOnInteraction: true,
            },
        });
        $(".stories_slider_container").mouseenter(function() {
            stories_slider_container.autoplay.stop();
        });

        $(".stories_slider_container").mouseleave(function() {
            stories_slider_container.autoplay.start();
        });
    }catch (e) {}

    try{
        (function() {
            'use strict';
            const breakpoint = window.matchMedia( '(min-width:769px)' );
            let mySwiper;
            const breakpointChecker = function() {
                try{
                    if ( breakpoint.matches === true ) {
                        if ( mySwiper !== undefined ) mySwiper.destroy( true, true );
                        return;
                    } else if ( breakpoint.matches === false ) {
                        return enableSwiper();
                    }
                }catch(e){console.log(e)}
            };

            const enableSwiper = function() {
                try{
                    mySwiper = new Swiper ('.album_poster_container', {
                        slidesPerView: 'auto',
                        spaceBetween: 9,
                        a11y: true,
                        keyboardControl: true,
                        // navigation: {
                        //     nextEl: '.post_rating_list_next',
                        // }
                    });
                }catch(e){console.log(e)}
            };
            breakpoint.addListener(breakpointChecker);
            breakpointChecker();
        })();
    }catch (e){}

    try{
        (function() {
            'use strict';
            const breakpoint = window.matchMedia( '(min-width:769px)' );
            let mySwiper;
            const breakpointChecker = function() {
                try{
                    if ( breakpoint.matches === true ) {
                        if ( mySwiper !== undefined ) mySwiper.destroy( true, true );
                        return;
                    } else if ( breakpoint.matches === false ) {
                        return enableSwiper();
                    }
                }catch(e){console.log(e)}
            };

            const enableSwiper = function() {
                try{
                    mySwiper = new Swiper ('.graphic_album_container', {
                        slidesPerView: 'auto',
                        spaceBetween: 20,
                        a11y: true,
                        keyboardControl: true,
                        // navigation: {
                        //     nextEl: '.post_rating_list_next',
                        // }
                    });
                }catch(e){console.log(e)}
            };
            breakpoint.addListener(breakpointChecker);
            breakpointChecker();
        })();
    }catch (e){}

    try{
        (function() {
            'use strict';
            const breakpoint = window.matchMedia( '(min-width:769px)' );
            let mySwiper;
            const breakpointChecker = function() {
                try{
                    if ( breakpoint.matches === true ) {
                        if ( mySwiper !== undefined ) mySwiper.destroy( true, true );
                        return;
                    } else if ( breakpoint.matches === false ) {
                        return enableSwiper();
                    }
                }catch(e){console.log(e)}
            };

            const enableSwiper = function() {
                try{
                    mySwiper = new Swiper ('.graphic_album_2_container', {
                        slidesPerView: 'auto',
                        spaceBetween: 20,
                        a11y: true,
                        keyboardControl: true,
                        // navigation: {
                        //     nextEl: '.post_rating_list_next',
                        // }
                    });
                }catch(e){console.log(e)}
            };
            breakpoint.addListener(breakpointChecker);
            breakpointChecker();
        })();
    }catch (e){}

    try{
        (function() {
            'use strict';
            const breakpoint = window.matchMedia( '(min-width:769px)' );
            let mySwiper;
            const breakpointChecker = function() {
                try{
                    if ( breakpoint.matches === true ) {
                        if ( mySwiper !== undefined ) mySwiper.destroy( true, true );
                        return;
                    } else if ( breakpoint.matches === false ) {
                        return enableSwiper();
                    }
                }catch(e){console.log(e)}
            };

            const enableSwiper = function() {
                try{
                    mySwiper = new Swiper ('.general_tape_container', {
                        slidesPerView: 'auto',
                        spaceBetween: 20,
                        a11y: true,
                        keyboardControl: true,
                        // navigation: {
                        //     nextEl: '.post_rating_list_next',
                        // }
                    });
                }catch(e){console.log(e)}
            };
            breakpoint.addListener(breakpointChecker);
            breakpointChecker();
        })();
    }catch (e){}

    $('[data-toggle]').on('click', function (e){
       $(this).toggleClass('active');
       $(`${$(this).attr('data-toggle')}`).toggleClass('show');
    });

    $(document).on('click','[data-play]',function (e){
        $(this).fadeOut(200);
        try {
            $(this).closest('.video_content').find('.video_info_top').fadeOut(200);
            $(this).closest('.video_content').find('.video_info_bottom').fadeOut(200);
        }catch (e){}
       $(`${$(this).attr('data-play')}`).trigger('play').attr('controls','controls');
    });

    $(document).on('click','[data-close]', function (e){
        $(`${$(this).attr('data-close')}`).fadeOut(250);
    })

    $(document).on('click', '.toggle_btn', function (e){
        $(this).toggleClass('active');
    });
});